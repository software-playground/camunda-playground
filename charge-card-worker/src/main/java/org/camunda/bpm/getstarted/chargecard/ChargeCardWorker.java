package org.camunda.bpm.getstarted.chargecard;

import org.camunda.bpm.client.ExternalTaskClient;
import java.util.logging.Logger;

public class ChargeCardWorker {

    private final static Logger LOGGER = Logger.getLogger(ChargeCardWorker.class.getName());

    public static void main(String[] args){
        ExternalTaskClient externalTaskClient = ExternalTaskClient
                .create().baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000)
                .build();

        externalTaskClient.subscribe("charge-card")
                .lockDuration(1000)
                .handler(((externalTask, externalTaskService) -> {
                    // Put you business logic here

                    // Get a process variable
                    String item = (String) externalTask.getVariable("item");
                    Long amount = (Long) externalTask.getVariable("amount");
                    LOGGER.info("Charging credit card with an amount of '" + amount +
                            "' Euros for item '" + item + "' ...");
                    externalTaskService.complete(externalTask);
                })).open();
    }
}
