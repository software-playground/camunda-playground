package org.camunda.bpm.paymentretrieval.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Logger;

public class UpdateOrderState implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger(UpdateOrderState.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        final long amount = (long) delegateExecution.getVariable("amount");
        final String item = (String) delegateExecution.getVariable("item");
        LOGGER.info("Start UpdateOrderState. amount = " + amount + ", item = " + item);

        final HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://localhost:9000/order/1/update"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        delegateExecution.setVariable("orderStateUpdateStatus", "SUCCESS");

        LOGGER.info("End UpdateOrderState. amount = " + amount + ", item = "
                + item + ", orderStateUpdateStatus = "
                +  delegateExecution.getVariable("orderStateUpdateStatus"));

    }
}
