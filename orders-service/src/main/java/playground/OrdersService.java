package playground;


import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrdersService {

    @GetMapping(value = "", produces = "application/json")
    public Map<String, Object> getOrder(){
        System.out.println("getOrder");
        Map<String, Object> response = new HashMap<>();
        response.put("id", 1);
        response.put("name", "aName");
        return response;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public Map<String, Object> getOrderById (@PathVariable("id") int id){
        System.out.println("getOrderById");
        Map<String, Object> response = new HashMap<>();
        response.put("id", 1);
        response.put("name", "aName");
        return response;
    }

    @GetMapping(value = "/{id}/update", produces = "application/json")
    public Map<String, String> updateOrderStatus (@PathVariable("id") int id){
        System.out.println("updateOrderStatus");
        Map<String, String> response = new HashMap<>();
        response.put("result", "success");
        return response;
    }

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public Map<String, String> addOrder (@RequestBody Object obj){
        System.out.println("addOrder.");
        Map<String, String> response = new HashMap<>();
        response.put("result", "success");
        return response;
    }

}
