package playground.camundaclient.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.Executors;

@Component
public class CamundaBPMRestServiceAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(CamundaBPMRestServiceAdapter.class);

    private HttpClient httpClient = createHttpClient();

    @Value("${bpm.endpoint}")
    private String BPM_ENDPOINT;

    @Value("${bpm.approvePaymentProcess.name}")
    private String BPM_APPROVE_PAYMENT_PROCESS_URL;

    @Value("${bpm.invokeProcess.suffix}")
    private String BPM_INVOKE_PROCESS_SUFFIX;

    public void approvePaymentProcess(PaymentProcessDto dto) throws Exception {
        LOGGER.debug("Start approvePaymentProcess");
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(new CamundaVariables(dto));
            final String processUri  = BPM_ENDPOINT + BPM_APPROVE_PAYMENT_PROCESS_URL + BPM_INVOKE_PROCESS_SUFFIX;
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(processUri))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            LOGGER.debug("Response status code: {}\nResponse headers: {}\nResponse body: ", response.statusCode(), response.headers(), response.body());
        } catch (Exception e) {
            e.printStackTrace();
            throw(e);
        }
    }

    private HttpClient createHttpClient() {
        CookieHandler.setDefault(new CookieManager());
        return HttpClient.newBuilder()
                .cookieHandler(CookieHandler.getDefault())
                .executor(Executors.newFixedThreadPool(1))
                .version(HttpClient.Version.HTTP_1_1)
                .build();
    }

    public static void main(String[] args) throws Exception {
        PaymentProcessDto dto = new PaymentProcessDto(123, "myItem");
        CamundaBPMRestServiceAdapter camundaBPMRestServiceAdapter = new CamundaBPMRestServiceAdapter();
        camundaBPMRestServiceAdapter.approvePaymentProcess(dto);
        System.exit(0);
    }
}
