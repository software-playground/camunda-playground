package playground.camundaclient.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CamundaVariables {
    private Object variables;

    public CamundaVariables(Object variables) {
        this.variables = variables;
    }

    public Object getVariables() {
        return variables;
    }

    public void setVariables(Object variables) {
        this.variables = variables;
    }

    public static void main(String... args) throws JsonProcessingException {
        PaymentProcessDto dto = new PaymentProcessDto(789, "myItem");
        CamundaVariables camundaVariables = new CamundaVariables(dto);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(camundaVariables);
        System.out.println(json);
    }
}
