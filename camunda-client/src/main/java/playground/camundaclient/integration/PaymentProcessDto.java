package playground.camundaclient.integration;

import java.util.HashMap;
import java.util.Map;

public class PaymentProcessDto {

    private Map<String, Object> amount = new HashMap<>();
    private Map<String, Object> item = new HashMap<>();

    public PaymentProcessDto(long amountVal, String itemVal) {
        amount.put("value", amountVal);
        amount.put("type", "long");
        item.put("value", itemVal);
        item.put("type", "string");
    }

    public Map<String, Object> getAmount() {
        return amount;
    }

    public void setAmount(Map<String, Object> amount) {
        this.amount = amount;
    }

    public Map<String, Object> getItem() {
        return item;
    }

    public void setItem(Map<String, Object> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PaymentProcessDto{");
        sb.append("amount=").append(amount);
        sb.append(", item=").append(item);
        sb.append('}');
        return sb.toString();
    }
}
