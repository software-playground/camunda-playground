package playground.camundaclient.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import playground.camundaclient.integration.CamundaBPMRestServiceAdapter;
import playground.camundaclient.integration.PaymentProcessDto;
import reactor.core.publisher.Mono;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;
import static reactor.core.publisher.Flux.just;

@Configuration
public class RouterFunctionConfig {

    @Autowired
    private CamundaBPMRestServiceAdapter camundaBPMRestServiceAdapter;

    @Bean
    public RouterFunction<?> routerFunction() {
        return RouterFunctions.route()
                .GET("/test", request -> ok().body(just("success"), String.class))
                .GET("/approvePaymentProcess", request -> approvePaymentProcess(request))
                .build();
    }

    private Mono<ServerResponse> approvePaymentProcess(ServerRequest request) {
        try {
            String amountStr = request.queryParam("amount").get();
            long amount = Long.parseLong(amountStr);
            String item = request.queryParam("item").get();
            PaymentProcessDto dto = new PaymentProcessDto(amount, item);
            camundaBPMRestServiceAdapter.approvePaymentProcess(dto);
            return ok().body(just("approvePaymentProcess success"), String.class);
        } catch (Exception e) {
            e.printStackTrace();
            return ok().body(just("approvePaymentProcess failure"), String.class);
        }
    }
}
