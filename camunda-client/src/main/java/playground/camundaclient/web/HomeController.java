package playground.camundaclient.web;


import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(){
        return "home";
    }

    @GetMapping("/home1")
    public Single<String> home1(){
        Single<String> obs = Single.just("home");
        return obs;
    }

}
